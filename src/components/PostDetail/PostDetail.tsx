import React from 'react';
import _ from 'lodash';
import { Grid, Typography, CircularProgress, withStyles } from '@material-ui/core';
import { AccessTime, Comment, AccountCircle } from '@material-ui/icons';
import moment from 'moment';
import { Redirect, RouteComponentProps } from 'react-router-dom';
import R from '../../utils/routes';
import isImage from '../../utils/isImage';
import styles from './PostDetail.styles';
import { IPost } from '../../interfaces';

interface IPostDetailProps {
  posts: IPost[];
}

interface IPostDetailState {
  imgLoaded: boolean;
}

export class PostDetail extends React.Component<
  IPostDetailProps & RouteComponentProps,
  IPostDetailState
> {
  state = {
    imgLoaded: false
  };

  componentDidUpdate(prevProps) {
    if (
      !!this.props.match.params.id &&
      !_.isEqual(this.props.match.params.id, prevProps.match.params.id)
    ) {
      this.setState({
        imgLoaded: false
      });
    }
  }
  imageLoaded = () => {
    this.setState({ imgLoaded: true });
  };

  render() {
    const { posts, match, classes } = this.props;
    const { imgLoaded } = this.state;
    const id = _.get(match, 'params.id');
    const post: IPost = posts && posts.find(post => post.id === id);
    if (!post) return <Redirect to={R.POST_NOT_FOUND} />;
    const haveImage = isImage(post.url);
    return (
      <div className={classes.root}>
        <Grid container spacing={2} justify="center">
          <Grid item xs={12} className={classes.imageContainer}>
            {haveImage && !imgLoaded && (
              <Grid container justify="center" alignItems="center" spacing={5}>
                <Grid item>
                  <CircularProgress color="inherit" />
                </Grid>
              </Grid>
            )}
            {haveImage ? (
              <img
                style={{ display: !imgLoaded ? 'none' : 'block' }}
                src={post.url}
                alt={post.title}
                className={classes.image}
                onLoad={this.imageLoaded}
              />
            ) : (
              <Typography variant="subtitle2" gutterBottom color="error">
                This post has no full size image
              </Typography>
            )}
          </Grid>
          <Grid className={classes.textContainer} container spacing={1} direction="column">
            <Grid item>
              <Grid container alignItems="center">
                <Grid item>
                  <AccountCircle className={classes.statusIcon} />
                </Grid>
                <Grid item>
                  <Typography variant="h5">{post.author}</Typography>
                </Grid>
              </Grid>
            </Grid>

            <Grid item>
              <Typography variant="h6"> &quot;{post.title}&quot;</Typography>
            </Grid>

            <Grid item>
              <Grid container>
                <Grid item>
                  <AccessTime className={classes.statusIcon} />
                </Grid>
                <Grid item>
                  <Typography variant="subtitle2">
                    {moment.unix(post.created_utc).fromNow()}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>

            <Grid item>
              <Grid container>
                <Grid item>
                  <Comment className={classes.statusIcon} />
                </Grid>
                <Grid item>
                  <Typography variant="subtitle2">{post.num_comments} comments</Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(PostDetail);
