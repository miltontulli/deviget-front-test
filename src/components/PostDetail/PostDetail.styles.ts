import { createStyles } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core';

export default createStyles((theme: Theme) => ({
  imageContainer: {
    display: 'flex',
    justifyContent: 'center'
  },
  image: {
    maxWidth: '100%',
    maxHeight: '50vh',
    height: 'auto',
    borderRadius: 4
  },
  textContainer: {
    padding: theme.spacing(2),
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(6)
    }
  },
  statusIcon: {
    fontSize: 20,
    marginRight: 5
  },
  root: {
    flexGrow: 1,
    padding: theme.spacing(2)
  }
}));
