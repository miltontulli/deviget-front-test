import React from 'react';
import { shallow } from 'enzyme';
import PostDetailWithStyles, { PostDetail } from './PostDetail';
import { posts } from '../../__mocks__';

describe('<PostDetail/>', () => {
  it('Should match Snapshot', () => {
    const wrapper = shallow(<PostDetailWithStyles post={posts} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('Should have default state', () => {
    const wrapper = shallow(<PostDetail post={posts} />);
    expect(wrapper.state('imgLoaded')).toEqual(false);
  });

  it('Should call setState con componentDidmount if prevprops.id !== nextProps.id', () => {
    const wrapper = shallow(<PostDetail post={posts} match={{ params: { id: posts[0].id } }} />);
    const setStateSpy = jest.spyOn(wrapper.instance(), 'setState');
    const didUpdateSpy = jest.spyOn(wrapper.instance(), 'componentDidUpdate');
    wrapper.setProps({ match: { params: { id: posts[1].id } } });
    expect(didUpdateSpy).toHaveBeenCalled();
    expect(setStateSpy).toHaveBeenCalled();
  });
});
