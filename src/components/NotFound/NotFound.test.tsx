import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import { NotFound } from './NotFound';
import { Typography } from '@material-ui/core';

describe('<NotFound>', () => {
  it('Should match snapshot', () => {
    const wrapper = renderer.create(<NotFound />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });

  it('Should contain - not found - text', () => {
    const wrapper = shallow(<NotFound />);
    const el = wrapper.find(Typography).first();
    expect(el.prop('children')).toEqual('Post Not Found');
  });
});
