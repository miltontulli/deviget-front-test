import React from 'react';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const styles = makeStyles(theme => ({
  text: {
    marginBottom: theme.spacing(2)
  },
  root: {
    padding: theme.spacing(4)
  }
}));

export const NotFound: React.FunctionComponent = () => {
  const classes = styles();
  return (
    <div className={classes.root}>
      <Typography className={classes.text} variant="h4">
        Post Not Found
      </Typography>
    </div>
  );
};
