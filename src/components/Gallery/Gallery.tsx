import * as React from 'react';
import ImageGallery from 'react-image-gallery';
import isEmpty from 'lodash/isEmpty';
import { IGalleryImage } from '../../interfaces';
import R from '../../utils/routes';
import { AppBar, Grid, Paper } from '@material-ui/core';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { useHistory } from 'react-router-dom';
import useStyles from './Gallery.styles';
import 'react-image-gallery/styles/css/image-gallery.css';

interface IGalleryProps {
  images: IGalleryImage[];
}

export const Gallery: React.FC<IGalleryProps> = (props: IGalleryProps) => {
  const history = useHistory();
  const classes = useStyles();
  const { images } = props;
  return (
    <>
      <AppBar position="static" className={classes.appBar}>
        <Toolbar>
          <Button variant="outlined" color="inherit" onClick={() => history.push(R.APP)}>
            <ChevronLeftIcon />
            Back to app
          </Button>
        </Toolbar>
      </AppBar>
      {isEmpty(images) ? (
        <Grid container justify="center">
          <Grid item>
            <Paper className={classes.paper}>
              <Typography variant="h5" gutterBottom>
                There are no images saved in gallery
              </Typography>
            </Paper>
          </Grid>
        </Grid>
      ) : (
        <ImageGallery items={images} />
      )}
    </>
  );
};
