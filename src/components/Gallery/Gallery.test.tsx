import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import { Gallery } from './Gallery';
import { posts } from '../../__mocks__';

const firstPost = posts[0];

describe('<Gallery>', () => {
  let props;

  beforeEach(() => {
    props = {
      images: [
        {
          original: firstPost.url,
          thumbnail: firstPost.thumbnail,
          originalTitle: firstPost.title,
          description: `${firstPost.author} - "${firstPost.title}"`
        }
      ]
    };
  });

  it('Should match snapshot', () => {
    const wrapper = renderer.create(<Gallery {...props} />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
  it('Should receive empty images without exploding', () => {
    const wrapper = shallow(<Gallery images={[]} />);
    expect(wrapper).toHaveLength(1);
  });
});
