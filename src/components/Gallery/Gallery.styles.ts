import { makeStyles } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    backgroundColor: theme.palette.primary.main,
    marginBottom: theme.spacing(2)
  },
  paper: {
    margin: theme.spacing(3),
    padding: theme.spacing(3)
  }
}));
