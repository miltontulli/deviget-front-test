import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import { Main } from './Main';
import { Link } from '@material-ui/core';

describe('<Main>', () => {
  it('Should match snapshot', () => {
    const wrapper = renderer.create(<Main />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });
  it('Should contain text', () => {
    const wrapper = shallow(<Main />);
    expect(wrapper.find(Link).prop('href')).toContain(
      'https://gitlab.com/miltontulli/deviget-front-test'
    );
  });
});
