import React from 'react';
import { Typography, Link } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  text: {
    marginBottom: theme.spacing(2)
  },
  root: {
    padding: theme.spacing(4)
  }
}));

export const Main: React.FunctionComponent = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Typography className={classes.text} variant="h4">
        Hey! This is my deviget-front-test solution
      </Typography>
      <Typography className={classes.text}>
        With the time I had, this is what I managed to do
      </Typography>
      <Typography className={classes.text}>
        To start open the menu and click on one of the left posts.
      </Typography>
      <Typography className={classes.text}>
        <Link href="https://gitlab.com/miltontulli/deviget-front-test">
          Gitlab repository: https://gitlab.com/miltontulli/deviget-front-test
        </Link>
      </Typography>
    </div>
  );
};
