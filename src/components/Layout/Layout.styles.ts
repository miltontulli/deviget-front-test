import { makeStyles } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core';

type StyleProps = { drawerWidth: number };

export default makeStyles<Theme, StyleProps>(theme => ({
  root: {
    display: 'flex',
    height: '100vh'
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    backgroundColor: theme.palette.primary.main
  },
  appBarShift: {
    width: ({ drawerWidth }) => `calc(100% - ${drawerWidth}px)`,
    marginLeft: ({ drawerWidth }) => drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  hide: {
    display: 'none'
  },
  content: {
    flexGrow: 1,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    marginLeft: ({ drawerWidth }) => `-${drawerWidth}px`,
    paddingTop: 64,
    [theme.breakpoints.down('xs')]: {
      paddingTop: 56
    }
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    }),
    marginLeft: '0 !important'
  }
}));
