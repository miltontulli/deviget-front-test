import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import { Layout } from './Layout';
import { posts } from '../../__mocks__';

describe('<Layout>', () => {
  let props;

  beforeEach(() => {
    props = {
      posts,
      isFetching: false
    };
  });

  it('Should match snapshot', () => {
    const wrapper = shallow(<Layout {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('Should call close click', async () => {
    const wrapper = shallow(<Layout {...props} />);
    const closeBtn = wrapper.find('[data-testid="onOpen"]');
    closeBtn.simulate('click');
    expect(closeBtn.prop('className')).toContain('makeStyles-hide-');
  });
});
