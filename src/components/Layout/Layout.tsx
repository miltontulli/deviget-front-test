import React from 'react';
import classNames from 'classnames';
import { AppBar, Toolbar, IconButton, Typography, Tooltip, Grid } from '@material-ui/core';
import { Menu as MenuIcon } from '@material-ui/icons';
import { ToastContainer } from 'react-toastify';
import { Drawer } from '../index';
import useStyles from './Layout.styles';
import { IPost } from '../../interfaces';
import { IDimensionsState, useDimensions } from '../useDimensions';
import { DRAWER_DEFAULT_WIDTH } from '../../utils/constants';
import CollectionsOutlined from '@material-ui/icons/CollectionsOutlined';
import DeleteForever from '@material-ui/icons/DeleteForever';
import Refresh from '@material-ui/icons/Refresh';
import { useHistory } from 'react-router-dom';
import R from '../../utils/routes';
import 'react-toastify/dist/ReactToastify.css';

interface ILayoutProps {
  children: React.ReactNode;
  posts: IPost[];
  isFetching: boolean;
  removePost: (postId: string) => void;
  readPost: (postId: string) => void;
  addToGallery: (postId: string) => void;
  getPosts: () => void;
  removeAllPosts: () => void;
}

export const Layout: React.FC<ILayoutProps> = (props: ILayoutProps) => {
  const {
    children,
    posts,
    isFetching,
    removePost,
    readPost,
    addToGallery,
    getPosts,
    removeAllPosts
  } = props;
  const history = useHistory();
  const dimensions: IDimensionsState = useDimensions();
  const drawerWidth = dimensions.isMobile ? dimensions.width : DRAWER_DEFAULT_WIDTH;
  const classes = useStyles({ drawerWidth });
  const [open, setOpen] = React.useState<boolean>(!dimensions.isMobile);

  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };

  const reset = () => {
    removeAllPosts();
    getPosts();
  };

  return (
    <div className={classes.root}>
      <AppBar
        position="fixed"
        className={classNames(classes.appBar, {
          [classes.appBarShift]: open
        })}
      >
        <Toolbar>
          <Grid container justify="space-between">
            <Grid item>
              <Grid container alignItems="center">
                <Grid item>
                  <IconButton
                    color="inherit"
                    data-testid="onOpen"
                    onClick={handleDrawerOpen}
                    edge="start"
                    className={classNames(classes.menuButton, open && classes.hide)}
                  >
                    <MenuIcon />
                  </IconButton>
                </Grid>
                {!open && (
                  <Grid item>
                    <Typography variant="h6" noWrap>
                      Reddit Posts
                    </Typography>
                  </Grid>
                )}
              </Grid>
            </Grid>
            <Grid item>
              <Tooltip title="View Gallery">
                <IconButton color="inherit" onClick={() => history.push(R.GALLERY)}>
                  <CollectionsOutlined className={classes.icon} />
                </IconButton>
              </Tooltip>
              <Tooltip title="Dismiss All Posts">
                <IconButton color="inherit" onClick={removeAllPosts}>
                  <DeleteForever className={classes.icon} />
                </IconButton>
              </Tooltip>
              <Tooltip title="Reset Posts">
                <IconButton color="inherit" onClick={reset}>
                  <Refresh className={classes.icon} />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <Drawer
        open={open}
        handleDrawerClose={handleDrawerClose}
        drawerWidth={drawerWidth}
        posts={posts}
        isFetching={isFetching}
        removePost={removePost}
        readPost={readPost}
        addToGallery={addToGallery}
        getPosts={getPosts}
      />
      <main
        className={classNames(classes.content, {
          [classes.contentShift]: open,
          [classes.hide]: open && dimensions.isMobile
        })}
      >
        {children}
      </main>

      <ToastContainer />
    </div>
  );
};
