import React from 'react';
import { shallow } from 'enzyme';
import { ListItem } from '@material-ui/core';
import { Post } from '..';
import { posts } from '../../__mocks__';

const firstPost = posts[0];
jest.useFakeTimers();
describe('<Post>', () => {
  let props;

  beforeEach(() => {
    props = {
      post: firstPost,
      removePost: jest.fn(),
      changeSelectedPost: jest.fn(),
      addPostToGallery: jest.fn()
    };
  });

  it('Should match snapshot', () => {
    const render = shallow(<Post {...props} />);
    expect(render).toMatchSnapshot();
  });

  it('Should call remove', () => {
    const wrapper = shallow(<Post {...props} />);
    const closeBtn = wrapper.find('[data-testid="remove"]').first();
    closeBtn.simulate('click');
    expect(props.removePost).not.toBeCalled();
    expect(setTimeout).toHaveBeenCalledTimes(1);
    jest.runAllTimers();
    expect(props.removePost).toHaveBeenCalled();
  });

  it('Should call select post', () => {
    const wrapper = shallow(<Post {...props} />);
    const item = wrapper.find(ListItem);
    item.simulate('click');
    expect(props.changeSelectedPost).toHaveBeenCalled();
    expect(props.changeSelectedPost).toHaveBeenCalledWith(props.post.id);
  });

  it('Should call add to gallery', () => {
    const wrapper = shallow(<Post {...props} />);
    const item = wrapper.find('[data-testid="addToGallery"]');
    item.simulate('click');
    expect(props.addPostToGallery).toHaveBeenCalled();
    expect(props.addPostToGallery).toHaveBeenCalledWith(props.post.id);
  });
});
