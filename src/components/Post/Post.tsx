import React, { useState } from 'react';
import classNames from 'classnames';
import moment from 'moment';
import {
  Avatar,
  IconButton,
  Typography,
  Tooltip,
  ListItemSecondaryAction,
  Menu,
  MenuItem,
  ListItemIcon,
  ListItemAvatar,
  ListItemText,
  Divider,
  ListItem
} from '@material-ui/core';
import {
  FiberManualRecordRounded as Readed,
  AddPhotoAlternate,
  Delete,
  MoreVert
} from '@material-ui/icons';
import { useTheme } from '@material-ui/core/styles';
import useStyles from './styles';
import { IPost } from '../../interfaces';

interface IPostProps {
  post: IPost;
  changeSelectedPost: (id: string) => void;
  removePost: (postId: string) => void;
  addPostToGallery: (id: string) => void;
}
export const Post: React.FC<IPostProps> = (props: IPostProps) => {
  const { post, changeSelectedPost, removePost, addPostToGallery } = props;
  const classes = useStyles();
  const theme = useTheme();

  const [dismissCard, setDismissCard] = useState(false);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClickOpenMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => setAnchorEl(null);

  const handleRemovePost = () => {
    handleMenuClose();
    setDismissCard(true);
    setTimeout(() => {
      removePost(post.id);
    }, theme.transitions.duration.shortest);
  };

  function handleSelectPostClick() {
    if (open) return handleMenuClose();
    changeSelectedPost(post.id);
  }

  const handleAddToGallery = () => {
    addPostToGallery(post.id);
    handleMenuClose();
  };
  return (
    <>
      <ListItem
        className={classNames(classes.post, dismissCard && classes.dismissPost)}
        alignItems="center"
        onClick={handleSelectPostClick}
      >
        <div className={classes.readStatus}>
          <Tooltip title={post.readed ? 'Readed Post' : 'Unreaded Post'}>
            <Readed
              className={classNames(classes.statusDot, {
                [classes.readed]: post.readed,
                [classes.unreaded]: !post.readed
              })}
            />
          </Tooltip>
        </div>
        <ListItemAvatar>
          <Avatar alt="Reddit post thumbnail" src={post.thumbnail || post.url} />
        </ListItemAvatar>
        <ListItemText
          className={classes.listText}
          primary={post.author}
          secondary={
            <React.Fragment>
              <Typography component="span" variant="body2" color="textPrimary">
                {post.title}
              </Typography>
              <Typography className={classes.timeAgo} component="span" variant="body2">
                {moment.unix(post.created_utc).fromNow()}
              </Typography>
              <Typography className={classes.timeAgo} component="span" variant="body2">
                {post.num_comments} comments
              </Typography>
            </React.Fragment>
          }
        />
        <ListItemSecondaryAction>
          <IconButton edge="end" aria-label="Options" onClick={handleClickOpenMenu}>
            <MoreVert />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
      <Menu id="long-menu" anchorEl={anchorEl} keepMounted open={open} onClose={handleMenuClose}>
        <MenuItem data-testid="remove" onClick={handleRemovePost}>
          <ListItemIcon className={classes.actionIcon}>
            <Delete fontSize="small" />
          </ListItemIcon>
          <Typography variant="inherit" noWrap>
            Delete
          </Typography>
        </MenuItem>

        <MenuItem data-testid="addToGallery" onClick={handleAddToGallery}>
          <ListItemIcon className={classes.actionIcon}>
            <AddPhotoAlternate fontSize="small" />
          </ListItemIcon>
          <Typography variant="inherit" noWrap>
            Add To Gallery
          </Typography>
        </MenuItem>
      </Menu>
      <Divider style={{ listStyle: 'none' }} variant="fullWidth" component="li" />
    </>
  );
};
