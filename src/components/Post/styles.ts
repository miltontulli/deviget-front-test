import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(({ palette, transitions, spacing }) => {
  return {
    post: {
      position: 'relative',
      transition: `all ${transitions.duration.complex}ms ${transitions.easing.easeOut}`,
      '&:hover': {
        cursor: 'pointer'
      }
    },
    dismissPost: {
      opacity: 0,
      transform: 'translate(-130px)'
    },
    timeAgo: {
      display: 'block',
      textAlign: 'left'
    },
    listText: {
      paddingRight: spacing(3)
    },
    actionIcon: {
      minWidth: 'unset',
      paddingRight: spacing(1) * 0.5
    },
    readStatus: {
      position: 'absolute',
      left: 3,
      top: -2
    },
    statusDot: {
      fontSize: 12
    },
    readed: {
      color: palette.grey[300]
    },
    unreaded: {
      color: palette.info.dark
    }
  };
});
