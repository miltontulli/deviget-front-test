import React from 'react';
import { shallow } from 'enzyme';
import { Drawer, Post } from '..';
import { posts } from '../../__mocks__';

describe('<Drawer>', () => {
  let props;

  beforeEach(() => {
    props = {
      open: true,
      handleDrawerClose: jest.fn(),
      drawerWidth: 260,
      posts: posts,
      isfetching: false
    };
  });

  it('Should match snapshot', () => {
    const render = shallow(<Drawer {...props} />);
    expect(render).toMatchSnapshot();
  });

  it('Should call close click', async () => {
    const wrapper = shallow(<Drawer {...props} />);
    const closeBtn = wrapper.find('[data-testid="close"]');
    closeBtn.simulate('click');
    expect(props.handleDrawerClose).toHaveBeenCalled();
  });

  it('Should render posts', async () => {
    const wrapper = shallow(<Drawer {...props} />);
    const postEl = wrapper.find(Post);
    expect(postEl.length).toEqual(posts.length);
  });
});
