import { makeStyles } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core';

type StyleProps = {
  drawerWidth: number;
};

export default makeStyles<Theme, StyleProps>(theme => ({
  drawer: {
    width: props => props.drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: props => props.drawerWidth,
    '&::-webkit-scrollbar': {
      display: 'none'
    }
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: 'space-between',
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white
  },
  buttonAction: {
    width: 200,
    margin: '30px auto',
    minHeight: 36
  },
  buttonLoading: {
    margin: '30px auto'
  }
}));
