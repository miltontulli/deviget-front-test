import React from 'react';
import classNames from 'classnames';
import {
  Divider,
  Typography,
  Tooltip,
  IconButton,
  Drawer as MUIDrawer,
  Button,
  List
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { Loading, Post, useDimensions, IDimensionsState } from '..';
import { IPost } from '../../interfaces';
import useStyles from './Drawer.styles';
import R from '../../utils/routes';
import { IS_MOBILE_BREAK } from '../../utils/constants';

interface IDrawerProps {
  className?: string;
  open: boolean;
  handleDrawerClose: () => void;
  drawerWidth: number;
  posts: IPost[];
  isFetching: boolean;
  removePost: (postId: string) => void;
  readPost: (postId: string) => void;
  addToGallery: (postId: string) => void;
  getPosts: () => void;
}

export const Drawer: React.FC<IDrawerProps> = (props: IDrawerProps) => {
  const {
    className,
    open,
    handleDrawerClose,
    drawerWidth,
    posts,
    isFetching,
    removePost,
    readPost,
    addToGallery,
    getPosts
  } = props;
  const classes = useStyles({ drawerWidth });
  const history = useHistory();
  const { isMobile, width }: IDimensionsState = useDimensions();

  const selectPost = (id: string) => (post: IPost) => {
    if (!post.readed) readPost(id);
    if (isMobile || width - drawerWidth < IS_MOBILE_BREAK) handleDrawerClose();
    history.push(`${R.POST}/${id}`);
  };

  return (
    <MUIDrawer
      className={classNames(classes.drawer, className)}
      variant="persistent"
      anchor="left"
      open={open}
      classes={{ paper: classes.drawerPaper }}
    >
      <div className={classes.drawerHeader}>
        <Typography variant="h6" noWrap>
          Reddit Posts
        </Typography>
        <Tooltip title="Close Draw bar">
          <IconButton data-testid="close" color="inherit" onClick={handleDrawerClose}>
            <ChevronLeftIcon color="inherit" />
          </IconButton>
        </Tooltip>
      </div>
      <Divider />
      <List disablePadding id="posts-container">
        {posts &&
          posts.map(post => (
            <Post
              key={post.id}
              post={post}
              changeSelectedPost={id => selectPost(id)(post)}
              removePost={removePost}
              addPostToGallery={(id: string) => addToGallery(id)}
            />
          ))}
      </List>
      <Loading isLoading={isFetching} className={classes.buttonLoading}>
        <Button
          id="btn-get-posts"
          variant="outlined"
          color="primary"
          className={classes.buttonAction}
          onClick={getPosts}
        >
          {posts.length > 0 ? 'Load More' : 'Load Posts'}
        </Button>
      </Loading>
    </MUIDrawer>
  );
};
