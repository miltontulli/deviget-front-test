import React from 'react';
import { Link } from 'react-router-dom';
import { Typography } from '@material-ui/core';
import R from '../../utils/routes';
import styles from './Landing.styles';

export const Landing = () => {
  const classes = styles();
  return (
    <div className={classes.root}>
      <Typography variant="h1" className={classes.header}>
        Reddit Client
      </Typography>
      <Link className={classes.link} to={R.APP}>
        <Typography variant="h4" className={classes.header}>
          Go to app
        </Typography>
      </Link>
    </div>
  );
};
