export interface IPost {
  title: string;
  thumbnail_height: number | null;
  thumbnail_width: number | null;
  author_fullname: string;
  name: string;
  total_awards_received: number;
  score: number;
  thumbnail?: string | null;
  created: number;
  created_utc: number;
  subreddit_id: string;
  id: string;
  author: string;
  num_comments: number;
  permalink: string;
  url: string;
  readed?: boolean;
}

export interface IGalleryImage {
  original: string;
  thumbnail: string;
  originalTitle: string;
  description: string;
}
