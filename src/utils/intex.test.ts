import extractPosts from './extractPosts';
import { posts } from '../__mocks__';
describe('utils/extractPost', () => {
  beforeEach(() => {});

  it('Should return empty arr if no params received', () => {
    const result = extractPosts();
    expect(result).toEqual([]);
  });

  it('Should formated posts', () => {
    const apiResponsePost = [
      {
        data: posts[0],
        kind: 't3'
      }
    ];
    const result = extractPosts(apiResponsePost);
    expect(apiResponsePost[0].data).toEqual(result[0]);
  });
});
