import { IReditWebResponse } from '../modules/posts/actions';
import { IPost } from '../interfaces';
import isEmpty from 'lodash/isEmpty';

/**
 * @function extractPosts
 * @description eliminate reddit web response extra level before each post.data
 * @param { posts } [{data: {...}}, {data: {...}}]
 * @returns { IPost[] } [ {...}, {...}]
 */
export default (posts?: IReditWebResponse<IPost>[]): IPost[] => {
  if (!posts || isEmpty(posts)) return [];
  return posts.map(p => p.data);
};
