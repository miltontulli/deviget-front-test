import axios from 'axios';
import queryString from 'query-string';
import env from '../config';

const API = axios.create({
  baseURL: env.baseUrl
});
interface Shape {
  [fieldName: string]: string;
}
const fetchPosts = (query: Shape = {}) => API.get(`/top/.json?${queryString.stringify(query)}`);
export { API as default, fetchPosts };
