/**
 * @function isImage
 * @description Return true/false if the urlString is a image or not
 * @param { urlString } string
 */
const isImage = (urlString: string): boolean => {
  return /\.(gif|jpe?g|tiff|png|webp|bmp)$/i.test(urlString);
};

export default isImage;
