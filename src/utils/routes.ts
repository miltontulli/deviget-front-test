export default {
  ROOT: '/',
  LANDING: '/landing',
  APP: '/app',
  POST: '/app/post',
  POST_NOT_FOUND: '/app/post/not-found',
  GALLERY: '/app/gallery'
};
