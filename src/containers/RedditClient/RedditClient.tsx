import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import { actions } from '../../modules/posts';
import { IState } from '../../modules';
import { IPost } from '../../interfaces';
import { Layout, NotFound, Main } from '../../components';
import { Switch, Route } from 'react-router-dom';
import R from '../../utils/routes';
import { PostDetail } from '../../components';

interface IRedditClientMapStateToProps {
  posts: IPost[];
  isFetching: boolean;
}
interface IRedditClientmapDispatchToProps {
  getPosts: actions.asyncThunkActionCreator;
  removePost: actions.IRemovePostAction;
  readPost: actions.readPostAction;
  addToGallery: actions.addGalleryPostAction;
  removeAllPosts: actions.Action;
}
interface IRedditClientProps
  extends IRedditClientMapStateToProps,
    IRedditClientmapDispatchToProps {}

export class RedditClient extends React.Component<IRedditClientProps> {
  componentDidMount() {
    if (isEmpty(this.props.posts)) this.props.getPosts();
    this.props.posts.forEach(post => {
      const newImage = new Image();
      newImage.src = post.url;
      newImage.id = post.id;
      window[post.id] = newImage;
    });
  }

  public render() {
    const {
      posts,
      isFetching,
      removePost,
      readPost,
      addToGallery,
      getPosts,
      removeAllPosts
    } = this.props;
    return (
      <Layout
        isFetching={isFetching}
        posts={posts}
        removePost={removePost}
        readPost={readPost}
        addToGallery={addToGallery}
        getPosts={getPosts}
        removeAllPosts={removeAllPosts}
      >
        <Switch>
          <Route exact path={R.POST_NOT_FOUND} component={NotFound} />
          <Route exact path={R.APP} component={Main} />

          <Route
            exact
            path={`${R.POST}/:id`}
            render={routerProps => <PostDetail {...routerProps} posts={posts} />}
          />
        </Switch>
      </Layout>
    );
  }
}

const mapStateToProps = (state: IState): IRedditClientMapStateToProps => ({
  posts: state.postsReducer.posts,
  isFetching: state.postsReducer.isFetching
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getPosts: actions.getPosts,
      removePost: actions.removePost,
      readPost: actions.readPost,
      addToGallery: actions.addToGallery,
      removeAllPosts: actions.removeAllPosts
    },
    dispatch
  );

export default connect<IRedditClientMapStateToProps, IRedditClientmapDispatchToProps>(
  mapStateToProps,
  mapDispatchToProps
)(RedditClient);
