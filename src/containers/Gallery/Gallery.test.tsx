import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import GalleryConnected, { Gallery } from './Gallery';
import getStore from '../../getStore';
import { posts } from '../../__mocks__';

describe('<App>', () => {
  let store;

  beforeEach(() => {
    store = getStore();
  });

  it('Should match snapshot', () => {
    const rendered = renderer
      .create(
        <Provider store={store}>
          <BrowserRouter>
            <GalleryConnected />
          </BrowserRouter>
        </Provider>
      )
      .toJSON();
    expect(rendered).toMatchSnapshot();
  });

  it('Should get gallery img with valid img url path', () => {
    const invalidUrlPostId = posts[0].id;
    const validUrlPost = posts[1];
    const wrapper = shallow(<Gallery posts={posts} gallery={[invalidUrlPostId]} />);
    expect((wrapper.instance() as Gallery).getGalleryImages()).toEqual([]);
    wrapper.setProps({ gallery: [validUrlPost.id] });
    const expectedValidImgGalleryOutput = [
      {
        original: validUrlPost.url,
        thumbnail: validUrlPost.thumbnail,
        originalTitle: validUrlPost.title,
        description: `${validUrlPost.author} - "${validUrlPost.title}"`
      }
    ];
    expect((wrapper.instance() as Gallery).getGalleryImages()).toEqual(
      expectedValidImgGalleryOutput
    );
  });
});
