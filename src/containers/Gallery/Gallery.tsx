import * as React from 'react';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import { IState } from '../../modules';
import { IPost, IGalleryImage } from '../../interfaces';
import { RouteComponentProps } from 'react-router-dom';
import isImage from '../../utils/isImage';
import { Gallery as GalleryComponent } from '../../components';

interface IGalleryMapStateToProps {
  posts: IPost[];
  gallery: string[];
}

interface IGalleryProps extends IGalleryMapStateToProps {}

export class Gallery extends React.Component<IGalleryProps & RouteComponentProps> {
  getGalleryImages = (): IGalleryImage[] => {
    const { posts, gallery } = this.props;
    return posts
      .filter(post => !isEmpty(post.url) && isImage(post.url) && gallery.includes(post.id))
      .map(post => {
        return {
          original: post.url,
          thumbnail: post.thumbnail,
          originalTitle: post.title,
          description: `${post.author} - "${post.title}"`
        };
      });
  };

  public render() {
    const images = this.getGalleryImages();
    return <GalleryComponent images={images} />;
  }
}

const mapStateToProps = (state: IState): IGalleryMapStateToProps => ({
  posts: state.postsReducer.posts,
  gallery: state.postsReducer.gallery
});

export default connect<IGalleryMapStateToProps>(mapStateToProps)(Gallery);
