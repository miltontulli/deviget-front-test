import defaultTo from 'lodash/defaultTo';

export default {
  baseUrl: defaultTo(process.env.REACT_APP_REDDIT_BASE_URL, 'https://www.reddit.com')
};
