import * as actions from './actions';
import reducer, { postsInitialState } from './reducer';
import * as types from './types';

export { reducer as default, postsInitialState, actions, types };
