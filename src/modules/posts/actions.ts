import get from 'lodash/get';
import { toast } from 'react-toastify';
import { ActionTypes } from './types';
import { fetchPosts } from '../../utils/api';
import { Dispatch } from 'redux';
import { IState } from '../index';
import { AxiosResponse } from 'axios';
import { IPost } from '../../interfaces';
import extractPosts from '../../utils/extractPosts';

const getPostsStart: Action = () => ({
  type: ActionTypes.GET_POSTS_START
});

const getPostsEnd: Action = () => ({
  type: ActionTypes.GET_POSTS_END
});

const getPostsSuccess = (posts: IPost[]): IPayloadAction<{ posts: IPost[] }> => ({
  type: ActionTypes.GET_POSTS_SUCCESS,
  payload: { posts }
});

const getPostsFail: Action = () => ({
  type: ActionTypes.GET_POSTS_FAIL
});

export const removePost: IRemovePostAction = (postId: string) => ({
  type: ActionTypes.REMOVE_POST,
  payload: { postId }
});

export const removeAllPosts: Action = () => ({
  type: ActionTypes.REMOVE_ALL_POSTS
});

const setAfterPost = (after: string): IPayloadAction<{ after: string }> => ({
  type: ActionTypes.SET_AFTER_POST,
  payload: { after }
});

export const getPosts: asyncThunkActionCreator = () => async (dispatch, getState) => {
  dispatch(getPostsStart());
  try {
    const state: IState = getState();
    const after = state.postsReducer.after;
    const response: AxiosResponse<IReditWebResponse<IRedditWebResponseData>> = await fetchPosts({
      after
    });
    const children = get(response, 'data.data.children', []);
    const parsedChildren = extractPosts(children);
    dispatch(setAfterPost(response.data.data.after));
    dispatch(getPostsSuccess(parsedChildren));
  } catch (error) {
    dispatch(getPostsFail());
  }
  dispatch(getPostsEnd());
};

export const readPost: readPostAction = (postId: string) => async (dispatch, getState) => {
  const state: IState = getState();
  const updatedPosts: IPost[] = state.postsReducer.posts.map((post: IPost) => {
    if (post.id === postId) {
      return {
        ...post,
        readed: true
      };
    }
    return post;
  });
  dispatch({
    type: ActionTypes.READ_POST,
    payload: { posts: updatedPosts }
  });
};

export const addToGallery: addGalleryPostAction = (postId: string) => async (
  dispatch,
  getState
) => {
  const state: IState = getState();
  const gallery: string[] = state.postsReducer.gallery;
  if (gallery.includes(postId)) return;
  dispatch({
    type: ActionTypes.SET_GALLERY_POSTS,
    payload: { gallery: [...gallery, postId] }
  });
  toast.info('Post added to gallery!', {
    position: 'top-right',
    autoClose: 2000,
    hideProgressBar: false,
    closeOnClick: false,
    pauseOnHover: false,
    draggable: true,
    progress: undefined
  });
};

interface IBaseAction {
  type: string;
}
interface IPayloadAction<P = any> extends IBaseAction {
  payload: P;
}
export type Action = () => IBaseAction;

export type IRemovePostAction = (postId: string) => IPayloadAction<{ postId: string }>;
export interface IReditWebResponse<T> {
  data: T;
  kind: string;
}
export interface IRedditWebResponseData {
  before: number;
  after: string;
  children: IReditWebResponse<IPost>[];
  dist: number;
  modhash: string;
}

export type asyncThunkActionCreator = () => (
  dispatch: Dispatch,
  getState: () => IState
) => Promise<void>;

export type readPostAction = (
  postId: string
) => (dispatch: Dispatch, getState: () => IState) => Promise<void>;

export type addGalleryPostAction = readPostAction;
