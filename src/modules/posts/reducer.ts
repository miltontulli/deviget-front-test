import update from 'immutability-helper';
import { IPost } from '../../interfaces';
import { ActionTypes } from './types';

export interface IPostReducerState {
  posts: IPost[];
  isFetching: boolean;
  gallery: string[];
  after: string;
}

export const postsInitialState: IPostReducerState = {
  posts: [],
  isFetching: false,
  gallery: [],
  after: ''
};

export interface IPostsPayload {
  posts: IPost[];
  postId: string;
  gallery: string[];
  after: '';
}

export default (
  state = postsInitialState,
  { type, payload }: { type: ActionTypes; payload: IPostsPayload }
) => {
  switch (type) {
    case ActionTypes.GET_POSTS_START:
      return update(state, {
        isFetching: { $set: true }
      });
    case ActionTypes.GET_POSTS_SUCCESS:
      return update(state, {
        posts: { $set: [...state.posts, ...payload.posts] }
      });
    case ActionTypes.GET_POSTS_END:
      return update(state, {
        isFetching: { $set: false }
      });
    case ActionTypes.REMOVE_POST:
      return update(state, {
        posts: arr => arr.filter(post => post.id !== payload.postId)
      });
    case ActionTypes.REMOVE_ALL_POSTS:
      return update(state, {
        posts: { $set: postsInitialState.posts },
        after: { $set: postsInitialState.after },
        gallery: { $set: postsInitialState.gallery }
      });
    case ActionTypes.READ_POST:
      return update(state, {
        posts: { $set: payload.posts }
      });
    case ActionTypes.SET_GALLERY_POSTS:
      return update(state, {
        gallery: { $set: payload.gallery }
      });
    case ActionTypes.SET_AFTER_POST:
      return update(state, {
        after: { $set: payload.after }
      });
    case ActionTypes.GET_POSTS_FAIL:
    default:
      return state;
  }
};
