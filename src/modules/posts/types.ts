export enum ActionTypes {
  GET_POSTS_START = 'posts/GET_POSTS_START',
  GET_POSTS_END = 'posts/GET_POSTS_END',
  GET_POSTS_SUCCESS = 'posts/GET_POSTS_SUCCESS',
  GET_POSTS_FAIL = 'posts/GET_POSTS_FAIL',
  REMOVE_POST = 'posts/REMOVE_POST',
  READ_POST = 'posts/READ_POST',
  SET_GALLERY_POSTS = 'posts/SET_GALLERY_POSTS',
  SET_AFTER_POST = 'posts/SET_AFTER_POST',
  REMOVE_ALL_POSTS = 'posts/REMOVE_ALL_POSTS'
}
