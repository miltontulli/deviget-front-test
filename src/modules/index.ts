import { combineReducers } from 'redux';
import posts from './posts';
import { IPostReducerState, postsInitialState } from './posts/reducer';

const mainReducer = combineReducers({
  postsReducer: posts
});

export interface IState {
  postsReducer: IPostReducerState;
}

export const initialState: IState = {
  postsReducer: postsInitialState
};

export default mainReducer;
