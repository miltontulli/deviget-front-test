import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { RedditClient, Gallery } from './containers';
import { Landing } from './components';
import R from './utils/routes';

function App() {
  return (
    <Switch>
      <Route exact path={R.LANDING} component={Landing}></Route>
      <Route exact path={R.GALLERY} component={Gallery} />
      <Route path={R.APP} component={RedditClient} />
      <Redirect from={R.ROOT} to={R.LANDING} />
    </Switch>
  );
}

export default App;
