# Deviget - React js test
___

> The static **website** will be hosted here 👉 [https://deviget-front-test.web.app/](https://deviget-front-test.web.app/). 
> You also can see **test coverage**  here 👉 [https://deviget-front-test-coverage.web.app/](https://deviget-front-test-coverage.web.app/) 
> Thanks to Firebase!🔥
You can find a video **demo** of the app working [**HERE**](https://drive.google.com/file/d/1Qlm8iOmHnhGJHu0SPbt3rap6xyHDMMXH/view?usp=sharing)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


## Setup 🔧
```
git clone https://gitlab.com/miltontulli/deviget-front-test
cd deviget-front-test
yarn install
```
## Available Scripts

In the project directory, you can run:
```
yarn start 
``` 
Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

 
```
yarn test 
``` 
Launches the test runner in the interactive watch mode.
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.


## Built with 🛠️
| Lib | URL |
| ------ | ------ |
| React | [https://es.reactjs.org/](https://es.reactjs.org/) |
| Typescript | [https://www.typescriptlang.org/](https://www.typescriptlang.org/) |
| Redux | [https://es.redux.js.org/](https://es.redux.js.org/) |
| Material-UI | [https://material-ui.com/](https://material-ui.com/) |
| Jest | [https://jestjs.io/docs/en/getting-started.html](https://jestjs.io/docs/en/getting-started.html) |
| Enzyme | [https://airbnb.io/enzyme/](https://airbnb.io/enzyme/) |

## Author ✒️
> **Milton Tulli**
milton.tulli@gmail.com
[https://www.linkedin.com/in/miltontulli/](https://www.linkedin.com/in/miltontulli/)

## Notes/Desitions:
- I use gitlab instead of github because I am used to working with gitlab pipelines
- As I didn't have a lot of time during the week due to my work I couldn't do small commits, but maybe a big one (since most of the work I did in a night or two)